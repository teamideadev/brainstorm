﻿// -----------------------------------------------------------------------------
//  <copyright file="CameraFollowScript.cs" company="Zack Loveless">
//      Copyright (c) Zack Loveless.  All rights reserved.
//  </copyright>
// -----------------------------------------------------------------------------

using System;
using System.Globalization;

using UnityEngine;

public class CameraFollowScript : MonoBehaviour
{
    private Camera mainCamera;
    private Vector3 velocity;

    public float dampTime   = 0.15f;
    public Transform target;
    public Vector3 offset   = new Vector3(0.5f, 0.35f, 0.0f);

    private PlayerController player;
    private bool isDead;

    [SerializeField]
    private float actualDampTime;

    public void Start()
    {
        mainCamera  = GetComponent<Camera>();
        isDead      = false;

        actualDampTime = dampTime;
    }

    public void Update()
    {
        // TODO: Get this to actually work. Right now there's a race condition - player respawns faster than the camera can reset/follow new player.
        actualDampTime = isDead ? 0.0f : dampTime;
    }

    public void FixedUpdate()
    {
        if (player != null)
        {
            velocity = player.GetVelocity();
        }

        // Credit: http://answers.unity3d.com/questions/29183/2d-cameraScript-smooth-follow.html#
        if (target != null)
        {
            Vector3 point       = mainCamera.WorldToViewportPoint( target.position );
            Vector3 delta       = target.position - mainCamera.ViewportToWorldPoint( new Vector3( offset.x, offset.y, point.z ) );
            Vector3 destination = transform.position + delta;

            SetPosition( Vector3.SmoothDamp( transform.position, destination, ref velocity, actualDampTime ) );
        }
    }

    public void SetIsDead(bool status)
    {
        isDead = status;
    }

    public void SetPlayer(PlayerController controller)
    {
        player = controller;
        target = controller.transform;
    }

    public void SetPosition(Vector3 position)
    {
        transform.position = position;
    }
}
