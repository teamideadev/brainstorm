﻿// -----------------------------------------------------------------------------
//  <copyright file="PlayerController.cs" company="Zack Loveless">
//      Copyright (c) Zack Loveless.  All rights reserved.
//  </copyright>
// -----------------------------------------------------------------------------

using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float speed      = 10f;
    public float jumpForce  = 700f;

    //public Transform groundCheckObject;
    //public LayerMask groundLayer;

    private Rigidbody rb;
    private bool isGrounded;
    //private float groundRadius = 0.2f;

    public Vector3 GetVelocity()
    {
        return rb.velocity;
    }

    public void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    public void Update()
    {
        if( isGrounded && Input.GetButtonDown( "Jump" ) )
        {
            isGrounded = false;
            rb.AddForce( new Vector3( 0f, jumpForce, 0f ) );
        }
    }

    public void FixedUpdate()
    {
        float movement = Input.GetAxis( "Horizontal" );

        rb.velocity = new Vector3( movement * speed, rb.velocity.y, 0f );
    }

    public void OnTriggerEnter( Collider other )
    {
        isGrounded = true;
    }

    public void OnTriggerExit( Collider other )
    {
        isGrounded = false;
    }
}
