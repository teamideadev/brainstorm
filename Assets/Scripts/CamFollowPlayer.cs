﻿using UnityEngine;
using System.Collections;

public class CamFollowPlayer : MonoBehaviour {
	public Transform target;
	public Vector3 offset;


	private Vector3 tempPos;

	// Use this for initialization
	void Start () {
		// This function intentionally left blank
	}
	
	// Update is called once per frame
	void Update () {
		// TODO: Recode to allow a smoother cameraScript pan that allows
		//		 The playerMask to wander the screen a bit before cameraScript follows.
		//		 (When coded, aim for along the screen's Rule of Thirds lines.)
		/*
        tempPos = transform.position;

		// Calculate the cameraScript position from Vector3 offsets.
		// 		Z is inverted so positive values give a broader viewing range.
		tempPos.x = target.position.x + offset.x;
		tempPos.y = target.position.y + offset.y;
		tempPos.z = target.position.z - offset.z;
		
		transform.position = tempPos;*/
	}
}
