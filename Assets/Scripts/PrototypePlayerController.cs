﻿// -----------------------------------------------------------------------------
//  <copyright file="PrototypePlayerController.cs" company="Zack Loveless">
//      Copyright (c) Zack Loveless.  All rights reserved.
//  </copyright>
// -----------------------------------------------------------------------------

using UnityEngine;

public class PrototypePlayerController : MonoBehaviour
{
    public float MaxSpeed = 10f;
    public float JumpForce = 700f;

    // The actual playerMask controller will use these. For now, the prototype is just a sphere which doesn't lend itself to using this "ground check" method (video).
    //public Transform groundCheckObject;
    //public LayerMask groundLayer;

    private Rigidbody _rigidbody;

    private bool _grounded;
    //private float _groundRadius = 0.2f;

    public Vector3 GetVelocity()
    {
        return _rigidbody.velocity;
    }

    public void Start()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }

    public void Update()
    {
        if (_grounded && Input.GetButtonDown("Jump"))
        {
            _grounded = false;
            _rigidbody.AddForce(new Vector3(0f, JumpForce, 0f));
        }
    }

    public void FixedUpdate()
    {
        float movement = Input.GetAxis("Horizontal");

        _rigidbody.velocity = new Vector3(movement * MaxSpeed, _rigidbody.velocity.y, 0f);
    }

    public void OnTriggerEnter(Collider other)
    {
        _grounded = true;
    }

    public void OnTriggerExit(Collider other)
    {
        _grounded = false;
    }
}
