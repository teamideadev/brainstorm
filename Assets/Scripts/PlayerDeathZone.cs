﻿// -----------------------------------------------------------------------------
//  <copyright file="PlayerDeathZone.cs" company="Zack Loveless">
//      Copyright (c) Zack Loveless.  All rights reserved.
//  </copyright>
// -----------------------------------------------------------------------------

using Atlantis.Unity;
using UnityEngine;

public class PlayerDeathZone : MonoBehaviour
{
    public LayerMask playerMask;
    public GameController gameController;
    public CameraFollowScript cameraScript;

    private bool isDead;

    public void LateUpdate()
    {
        isDead = false;
    }

    public void OnTriggerEnter(Collider other)
    {
        if (playerMask.Contains(other.gameObject.layer))
        {
            if( isDead ) return;

            cameraScript.SetIsDead( true );

            isDead = true;
            Destroy(other.gameObject);
            gameController.Respawn();

            cameraScript.SetIsDead( false );
        }
    }
}
