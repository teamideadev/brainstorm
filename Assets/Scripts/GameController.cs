﻿// -----------------------------------------------------------------------------
//  <copyright file="GameController.cs" company="Zack Loveless">
//      Copyright (c) Zack Loveless.  All rights reserved.
//  </copyright>
// -----------------------------------------------------------------------------

using UnityEngine;

public class GameController : MonoBehaviour
{
    private CameraFollowScript cameraScript;
    public Transform playerPrefab;
    public Transform spawner;
    
    private Transform player;

    public void Start()
    {
        cameraScript = Camera.main.GetComponent<CameraFollowScript>();
        Respawn();
    }

    public void Respawn()
    {
        var temp = Instantiate(playerPrefab, spawner.position, spawner.rotation) as Transform;
        if (temp != null)
        {
            player = temp;

            var controller = temp.GetComponent<PlayerController>();
            if (controller != null)
            {
                cameraScript.SetPlayer(controller);
            }
        }
    }
}
