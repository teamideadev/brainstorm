﻿// -----------------------------------------------------------------------------
//  <copyright file="SnapToGridEditor.cs" company="Zack Loveless">
//      Copyright (c) Zack Loveless.  All rights reserved.
//  </copyright>
// -----------------------------------------------------------------------------

using UnityEditor;

using UnityEngine;

using Debug = System.Diagnostics.Debug;

[InitializeOnLoad]
[CustomEditor(typeof(SnapToGrid), true)]
[CanEditMultipleObjects]
public class SnapToGridEditor : Editor
{
    #region Overrides of Editor

    /// <summary>
    /// <para>
    /// Implement this function to make a custom inspector.
    /// </para>
    /// </summary>
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        SnapToGrid actor = target as SnapToGrid;
        Debug.Assert(actor != null, "actor != null");

        if (actor.snapToGrid)
        {
            actor.transform.position = RoundTransform(actor.transform.position, actor.snapValue);
        }

        if (actor.sizeToGrid)
        {
            actor.transform.localScale = RoundTransform(actor.transform.localScale, actor.sizeValue);
        }
    }

    #endregion

    private Vector3 RoundTransform(Vector3 vector, float value)
    {
        return new Vector3
            (
                value * Mathf.Round(vector.x / value),
                value * Mathf.Round(vector.y / value),
                vector.z
            );
    }
}
